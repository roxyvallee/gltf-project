#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec3 vTexCoords;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

out vec3 fColor;

void main() 
{
    vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
    float valuePi = 1. / 3.14;
    fColor = vec3(valuePi) * uLightIntensity * dot(viewSpaceNormal,uLightDirection);
}
